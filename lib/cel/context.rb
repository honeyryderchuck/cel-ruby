# frozen_string_literal: true

module Cel
  class Context
    attr_reader :declarations

    def initialize(declarations, bindings)
      @declarations = declarations
      @bindings = bindings.dup

      return unless @bindings

      @bindings.each do |k, v|
        val = to_cel_type(v)

        if @declarations && (type = @declarations[k])
          type = TYPES[type] if type.is_a?(Symbol)

          val = type.cast(val)
        end
        @bindings[k] = val
      end
    end

    def lookup(identifier)
      raise EvaluateError, "no value in context for #{identifier}" unless @bindings

      id = identifier.id

      lookup_keys = id.split(".").map(&:to_sym)

      val = @bindings
      lookup_keys.each do |key|
        raise EvaluateError, "no value in context for #{id}" unless val.key?(key)

        val = val[key]
      end

      val
    end

    def merge(bindings)
      Context.new(@declarations, @bindings ? @bindings.merge(bindings) : bindings)
    end

    private

    def to_cel_type(v)
      Literal.to_cel_type(v)
    end
  end
end
