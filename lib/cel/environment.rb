# frozen_string_literal: true

module Cel
  class Environment
    def initialize(declarations: nil, container: nil, disable_check: false)
      @disable_check = disable_check
      @declarations = declarations
      @container = nil
      @checker = Checker.new(@declarations)

      if container && !container.empty?

        module_dir = container.split(".")

        Dir[File.join(*module_dir, "*.rb")].each do |path|
          require path
        end

        package = Object.const_get(
          container.split(".").map do |sub|
            sub.capitalize.gsub(/_([a-z\d]*)/) do
              ::Regexp.last_match(1).capitalize
            end
          end.join("::")
        )

        @container = container
      end

      @parser = Parser.new(package)
    end

    def compile(expr)
      ast = @parser.parse(expr)
      @checker.check(ast)
      ast
    end

    def encode(expr)
      Encoder.encode(compile(expr))
    end

    def decode(encoded_expr)
      ast = Encoder.decode(encoded_expr)
      @checker.check(ast) unless @disable_check
      ast
    end

    def check(expr)
      ast = @parser.parse(expr)
      @checker.check(ast)
    end

    def program(expr)
      expr = @parser.parse(expr) if expr.is_a?(::String)
      @checker.check(expr) unless @disable_check
      Runner.new(@declarations, expr)
    end

    def evaluate(expr, bindings = nil)
      context = Context.new(@declarations, bindings)
      expr = @parser.parse(expr) if expr.is_a?(::String)
      @checker.check(expr) unless @disable_check
      Program.new(context).evaluate(expr)
    end

    private

    def validate(ast, structs); end
  end

  class Runner
    def initialize(declarations, ast)
      @declarations = declarations
      @ast = ast
    end

    def evaluate(bindings = nil)
      context = Context.new(@declarations, bindings)
      Program.new(context).evaluate(@ast)
    end
  end
end
