# frozen_string_literal: true

require "google/protobuf/struct_pb"
require "google/protobuf/wrappers_pb"
require "google/protobuf/any_pb"
require "google/protobuf/well_known_types"

module Cel
  module Protobuf
    PROTO_TO_CEL_TYPE = {
      Google::Protobuf::Any => TYPES[:any],
      Google::Protobuf::Value => TYPES[:any],
      Google::Protobuf::ListValue => TYPES[:list],
      Google::Protobuf::Struct => TYPES[:map],
      Google::Protobuf::BoolValue => TYPES[:bool],
      Google::Protobuf::BytesValue => TYPES[:bytes],
      Google::Protobuf::DoubleValue => TYPES[:double],
      Google::Protobuf::FloatValue => TYPES[:double],
      Google::Protobuf::Int32Value => TYPES[:int],
      Google::Protobuf::Int64Value => TYPES[:int],
      Google::Protobuf::UInt32Value => TYPES[:uint],
      Google::Protobuf::UInt64Value => TYPES[:uint],
      Google::Protobuf::NullValue => TYPES[:null],
      Google::Protobuf::StringValue => TYPES[:string],
      Google::Protobuf::Timestamp => TYPES[:timestamp],
      Google::Protobuf::Duration => TYPES[:duration],
    }.freeze

    module_function

    def base_class
      Google::Protobuf::MessageExts
    end

    def convert_from_protobuf(msg)
      case msg
      when Google::Protobuf::Any
        type_msg = const_get(package_to_module_name(msg.type_name))
        convert_from_protobuf(msg.unpack(type_msg))
      when Google::Protobuf::ListValue
        msg.to_a
      when Google::Protobuf::Struct
        msg.to_h
      when Google::Protobuf::Value
        msg.to_ruby(true)
      when Google::Protobuf::BytesValue
        Cel::Bytes.new(msg.value.bytes)
      when Google::Protobuf::Int32Value,
          Google::Protobuf::Int64Value
        Cel::Number.new(:int, msg.value)
      when Google::Protobuf::UInt32Value,
        Google::Protobuf::UInt64Value
        Cel::Number.new(:unit, msg.value)
      when Google::Protobuf::DoubleValue,
            Google::Protobuf::FloatValue
        Cel::Number.new(:double, msg.value)
      when Google::Protobuf::BoolValue,
           Google::Protobuf::NullValue,
           Google::Protobuf::StringValue
        # conversion to cel type won't be ambiguous here
        msg.value
      when Google::Protobuf::Timestamp
        msg.to_time
      when Google::Protobuf::Duration
        Cel::Duration.new(seconds: msg.seconds, nanos: msg.nanos)
      else
        msg.extend(CelComparisonMode)
        msg
      end
    end

    def convert_to_proto_type(type, package)
      proto_type = package_to_module_name(type)

      return Object.const_get(proto_type) if Object.const_defined?(proto_type)

      return unless package

      return unless package.const_defined?(proto_type)

      package.const_get(proto_type)
    end

    def convert_to_cel_type(proto)
      PROTO_TO_CEL_TYPE[proto]
    end

    def convert_to_proto(val, values)
      proto_type = val.message_type

      if proto_type == Google::Protobuf::Struct
        proto_type.from_hash(values[:fields])
      elsif proto_type == Google::Protobuf::ListValue
        proto_type.from_a(values[:values])
      elsif proto_type == Google::Protobuf::NullValue
        nil
      elsif proto_type == Google::Protobuf::Value
        if values.key?(:struct_value)
          values[:struct_value] = Google::Protobuf::Struct.from_hash(values[:struct_value])
        elsif values.key?(:list_value)
          values[:list_value] = Google::Protobuf::Struct.from_a(values[:list_value])
        elsif values.empty?
          values[:null_value] = Google::Protobuf::NullValue::NULL_VALUE
        end

        proto_type.new(values)
      else
        proto_type.new(values)
      end
    end

    def try_invoke_from(var, func, args)
      case var
      when "Any", "google.protobuf.Any",
        "ListValue", "google.protobuf.ListValue",
        "Struct", "google.protobuf.Struct",
        "Value", "google.protobuf.Value",
        "BoolValue", "google.protobuf.BoolValue",
        "BytesValue", "google.protobuf.BytesValue",
        "DoubleValue", "google.protobuf.DoubleValue",
        "FloatValue", "google.protobuf.FloatValue",
        "Int32Value", "google.protobuf.Int32Value",
        "Int64Value", "google.protobuf.Int64Value",
        "Uint32Value", "google.protobuf.Uint32Value",
        "Uint64Value", "google.protobuf.Uint64Value",
        "NullValue", "google.protobuf.NullValue",
        "StringValue", "google.protobuf.StringValue",
        "Timestamp", "google.protobuf.Timestamp",
        "Duration", "google.protobuf.Duration"
        protoclass = var.split(".").last
        protoclass = Google::Protobuf.const_get(protoclass)

        value = if args.nil? && protoclass.constants.include?(func.to_sym)
          protoclass.const_get(func)
        else
          protoclass.__send__(func, *args)
        end

        Literal.to_cel_type(value)
      end
    end

    def package_to_module_name(type)
      namespace, type_msg = type.split("/", 2)
      type_msg ||= namespace # sometimes there's no namespace
      type_msg.split(".").map do |typ|
        /[[:upper:]]/.match?(typ[0]) ? typ : typ.capitalize
      end.join("::")
    end

    module CelComparisonMode
      def ==(other)
        super || begin
          return false unless other.is_a?(Protobuf.base_class)

          a1 = self
          a2 = other

          if a1.is_a?(Google::Protobuf::Any) && !a1.type_url.empty?
            a1 = a1.unpack(Object.const_get(Protobuf.package_to_module_name(a1.type_name)))
            a1.extend(CelComparisonMode)
          end

          if a2.is_a?(Google::Protobuf::Any) && !a2.type_url.empty?
            a2 = a2.unpack(Object.const_get(Protobuf.package_to_module_name(a2.type_name)))
          end

          return false unless a1.class == a2.class

          if a1.is_a?(Google::Protobuf::Any) && a1.type_url.empty?
            a2.is_a?(Google::Protobuf::Any) && a2.type_url.empty?
            return a1.value == a2.value
          end

          a1.class.descriptor.each do |field|
            f1 = a1[field.name]
            f1.extend(CelComparisonMode) if f1.is_a?(Protobuf.base_class)
            return false unless f1 == a2[field.name]
          end
          true
        end
      end
    end
  end
end
