# frozen_string_literal: true

module Cel
  class Program
    attr_reader :context

    def initialize(context)
      @context = context
      @convert_to_cel = true
    end

    def initialize_dup(other)
      super
      @context = orig.instance_variable_get(:@context).dup
    end

    def with_extra_context(context)
      self.class.new(@context.merge(context))
    end

    def evaluate(ast)
      case ast
      when Group
        evaluate(ast.value)
      when Invoke
        evaluate_invoke(ast)
      when Operation
        evaluate_operation(ast)
      when Message
        evaluate_message(ast)
      when Literal
        evaluate_literal(ast)
      when Identifier
        evaluate_identifier(ast)
      when Condition
        evaluate_condition(ast)
      end
    end

    alias_method :call, :evaluate

    private

    def evaluate_identifier(identifier)
      if Cel::PRIMITIVE_TYPES.include?(identifier.to_sym)
        TYPES[identifier.to_sym]
      else
        @context.lookup(identifier)
      end
    end

    def evaluate_literal(val)
      case val
      when List
        List.new(val.value.map(&method(:call)))
      else
        val
      end
    end

    def evaluate_message(val)
      convert_to_cel = @convert_to_cel

      @convert_to_cel = false
      values = val.struct.transform_values do |value|
        v = call(value)
        case v
        when Bytes
          v.string
        when Map
          v.value.to_h { |k, v| [k.to_s, v.value] }
        when List
          v.value.map(&:value)
        when Literal
          v.value
        else
          v
        end
      end
      value = Protobuf.convert_to_proto(val, values)

      return value unless convert_to_cel

      Literal.to_cel_type(value)
    ensure
      @convert_to_cel = convert_to_cel
    end

    def evaluate_operation(operation)
      op = operation.op

      operands = operation.operands

      if operation.unary? &&
         op != "!" # https://bugs.ruby-lang.org/issues/18246
        # unary operations
        Literal.to_cel_type(call(operands.first).__send__(:"#{op}@"))
      elsif op == "&&"
        Bool.cast(
          operands.all? do |x|
            true == call(x).value # rubocop:disable Style/YodaCondition
          end
        )
      elsif op == "||"
        Bool.cast(
          operands.any? do |x|
            true == call(x).value # rubocop:disable Style/YodaCondition
          rescue StandardError
            false
          end
        )
      elsif op == "in"
        element, collection = operands
        Bool.cast(call(collection).include?(call(element)))
      else
        op_value, *values = operands.map(&method(:call))
        val = op_value.public_send(op, *values)

        Literal.to_cel_type(val)
      end
    end

    def evaluate_invoke(invoke, var = invoke.var)
      func = invoke.func
      args = invoke.args

      return evaluate_standard_func(invoke) unless var

      var = case var
            when Identifier
              evaluate_identifier(var)
            when Invoke
              evaluate_invoke(var)
            else
              var
      end

      case var
      when String
        raise EvaluateError, "#{invoke} is not supported" unless String.method_defined?(func, false)

        var.public_send(func, *args.map(&method(:call)))
      when Message
        var = evaluate_message(var)
        # If e evaluates to a message and f is not declared in this message, the
        # runtime error no_such_field is raised.
        raise NoSuchFieldError.new(var, func) unless var.respond_to?(func)

        Literal.to_cel_type(var.public_send(func))
      when Map, List
        return Macro.__send__(func, var, *args, program: self) if Macro.respond_to?(func)

        # If e evaluates to a map, then e.f is equivalent to e['f'] (where f is
        # still being used as a meta-variable, e.g. the expression x.foo is equivalent
        # to the expression x['foo'] when x evaluates to a map).

        args ?
        var.public_send(func, *args) :
        var.public_send(func)
      when Timestamp, Duration
        raise EvaluateError, "#{invoke} is not supported" unless var.class.method_defined?(func, false)

        var.public_send(func, *args)
      else
        raise EvaluateError, "#{invoke} is not supported"
      end
    end

    def evaluate_condition(condition)
      call(condition.if).value ? call(condition.then) : call(condition.else)
    end

    def evaluate_standard_func(funcall)
      func = funcall.func
      args = funcall.args

      case func
      when :type
        val = call(args.first)
        return val.type if val.respond_to?(:type)

        val.class
      # MACROS
      when :has, :matches, :size
        Macro.__send__(func, *args, program: self)
      when :int, :uint, :string, :double, :bytes, :duration, :timestamp
        type = TYPES[func]
        type.cast(call(args.first))
      when :dyn
        call(args.first)
      else
        return evaluate_custom_func(@context.declarations[func], funcall) if @context.declarations.key?(func)

        raise EvaluateError, "#{funcall} is not supported"
      end
    end

    def evaluate_custom_func(func, funcall)
      args = funcall.args

      func.call(*args.map(&method(:call)).map(&:to_ruby_type))
    end
  end
end
