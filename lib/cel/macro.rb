# frozen_string_literal: true

module Cel
  module Macro
    module_function

    # If e evaluates to a protocol buffers version 2 message and f is a defined field:
    #     If f is a repeated field or map field, has(e.f) indicates whether the field is non-empty.
    #     If f is a singular or oneof field, has(e.f) indicates whether the field is set.
    # If e evaluates to a protocol buffers version 3 message and f is a defined field:
    #     If f is a repeated field or map field, has(e.f) indicates whether the field is non-empty.
    #     If f is a oneof or singular message field, has(e.f) indicates whether the field is set.
    #     If f is some other singular field, has(e.f) indicates whether the field's value is its default
    #       value (zero for numeric fields, false for booleans, empty for strings and bytes).
    def has(invoke, program:)
      var = invoke.var
      func = invoke.func

      case var
      when Message
        var = program.evaluate(var)
        # If e evaluates to a message and f is not a declared field for the message,
        # has(e.f) raises a no_such_field error.
        raise NoSuchFieldError.new(var, func) unless var.respond_to?(func)

        Bool.cast(!var.public_send(func).nil?)
      when Map
        # If e evaluates to a map, then has(e.f) indicates whether the string f
        # is a key in the map (note that f must syntactically be an identifier).
        Bool.cast(var.respond_to?(func))
      else
        # In all other cases, has(e.f) evaluates to an error.
        raise EvaluateError, "#{invoke} is not supported"
      end
    end

    def size(literal, program: nil)
      literal = program.evaluate(literal) if program
      Cel::Number.new(:int, program.evaluate(literal).size)
    end

    def matches(string, pattern, program: nil)
      pattern = program.evaluate(pattern) if program
      pattern = Regexp.new(pattern)
      Bool.cast(pattern.match?(string))
    end

    def all(collection, identifier, predicate, program:)
      return_value = collection.all? do |element, *|
        program.with_extra_context(identifier.to_sym => element).evaluate(predicate).value
      end
      Bool.cast(return_value)
    end

    def exists(collection, identifier, predicate, program:)
      return_value = collection.any? do |element, *|
        program.with_extra_context(identifier.to_sym => element).evaluate(predicate).value
      end
      Bool.cast(return_value)
    end

    def exists_one(collection, identifier, predicate, program:)
      return_value = collection.one? do |element, *|
        program.with_extra_context(identifier.to_sym => element).evaluate(predicate).value
      end
      Bool.cast(return_value)
    end

    def filter(collection, identifier, predicate, program:)
      collection.select do |element, *|
        program.with_extra_context(identifier.to_sym => element).evaluate(predicate).value
      end
    end

    def map(collection, identifier, predicate, program:)
      collection.map do |element, *|
        program.with_extra_context(identifier.to_sym => element).evaluate(predicate)
      end
    end
  end
end
