# frozen_string_literal: true

require "bundler/gem_tasks"
require "rake/testtask"
require "minitest/test_task"

# https://stackoverflow.com/questions/2108727/which-in-ruby-checking-if-program-exists-in-path-from-ruby
def which(cmd)
  exts = ENV["PATHEXT"] ? ENV["PATHEXT"].split(";") : [""]
  ENV["PATH"].split(File::PATH_SEPARATOR).each do |path|
    exts.each do |ext|
      exe = File.join(path, "#{cmd}#{ext}")
      return exe if File.executable?(exe) && !File.directory?(exe)
    end
  end
  nil
end

desc "builds protobufs stubs for testing"
task :build_test_protos do
  test_protos_dir = File.expand_path("test", __dir__)
  test_protos = Dir.glob(File.join(test_protos_dir, "*.proto"))

  test_protos.each do |proto_file|
    puts "generating stub for #{proto_file}..."
    sh "grpc_tools_ruby_protoc -I#{test_protos_dir} --ruby_out=test --experimental_allow_proto3_optional #{proto_file}"
  end
end

Rake::TestTask.new do |t|
  t.libs = %w[lib test]
  t.pattern = "test/*_test.rb"
  t.warning = false
end

Minitest::TestTask.create(:conformance) do |t|
  t.libs = %w[conformance lib test .]
  t.test_globs = %w[conformance/conformance_test.rb]
end

begin
  require "rubocop/rake_task"
  desc "Run rubocop"
  RuboCop::RakeTask.new
rescue LoadError # rubocop:disable Lint/SuppressedException
end

namespace :coverage do
  desc "Aggregates coverage reports"
  task :report do
    return unless ENV.key?("CI")

    require "simplecov"

    SimpleCov.minimum_coverage 85

    SimpleCov.collate Dir["coverage/**/.resultset.json"]
  end
end

task default: %i[test]
